INCDIR		= include
SRCDIR 		= src
BUILD_DIR 	= build

EXECUTABLE	= TriTest

CXXFLAGS  	= -I$(INCDIR) -Wall -Wextra -pedantic -std=c++11

_OBJ		= Test.o Point.o Triangle.o
OBJ			= $(patsubst %,$(BUILD_DIR)/%,$(_OBJ))
_DEPS		= Triangle.h Point.h
DEPS		= $(patsubst %,$(INCDIR)/%,$(_DEPS))

# Build everything, alias of building the file TriTest
.PHONY: all
all: $(EXECUTABLE)

# Build the executable, link all objects
$(EXECUTABLE): $(OBJ) | $(BUILD_DIR)
	@echo "\n------------------------------------------------------"
	@echo "\tBuilding executable:" $@
	@echo "------------------------------------------------------"
	$(CXX) -o $@ $^ $(CXXFLAGS)
	@echo "------------------------------------------------------"
	@echo $@ "Done"

# Compile an object file in the build directory from the cpp
# file the source directory
$(BUILD_DIR)/%.o: $(SRCDIR)/%.cpp $(DEPS) $(BUILD_DIR)
	@echo "\n------------------------------------------------------"
	@echo "\tCompiling:" $<
	@echo "------------------------------------------------------"
	$(CXX) -c -o $@ $< $(CXXFLAGS)
	@echo "------------------------------------------------------"
	@echo $@ "Done"

# Alias of building Point and Triangle
.PHONY: Point
Point: $(BUILD_DIR)/Point.o
.PHONY: Triangle
Triangle: $(BUILD_DIR)/Triangle.o

# Create build directory if it doesn't exist
$(BUILD_DIR):
	@echo "\n------------------------------------------------------"
	@echo "Creating build directory:" $@
	@echo "------------------------------------------------------"
	mkdir $@

# Clean up
.PHONY: clean
clean:
	@echo "\n------------------------------------------------------"
	@echo "\tClean up "
	@echo "------------------------------------------------------"
	rm -f $(BUILD_DIR)/*.o $(EXECUTABLE)
